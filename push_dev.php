<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	include 'opendb.php';
	
	$res = array();
	$newResult = array();
	$newResultSC = array();
	$payload_fixed = preg_replace( "/\r|\n/", " ", $_REQUEST['payload'] );
	$payload = json_decode($payload_fixed,true);
	$registrationIDs = $_REQUEST['regId'];
	$auth = false;
	
	if(isset($_REQUEST['destinatario'])) {
		$destinatario = $_REQUEST['destinatario'];
	}else{
		$destinatario = "N/A";
	}
	if(isset($_REQUEST['hms'])){
		$hms = ($_REQUEST['hms'] == "true");
	}else{
		$hms = false;
	}
	if(isset($_REQUEST['platform'])){
		$platform = $_REQUEST['platform'];
	}else{
		$platform = "old";
	}
	if(isset($_REQUEST['os'])){
		$push_os = $_REQUEST['os'];
	}else{
		$push_os = "android";
	}

	if(isset($_REQUEST['token'])){
		$token = $_REQUEST['token'];
	}else{
		$token = "-";
	}
	if(isset($_REQUEST['image'])){
		$image = $_REQUEST['image'];
	}else{
		$image = null;
	}
	if(isset($_REQUEST['authKey'])){
		$authKey = $_REQUEST['authKey'];
	}else{
		$authKey = null;
	}
	if(isset($_REQUEST['p256dh'])){
		$p256dh = $_REQUEST['p256dh'];
	}else{
		$p256dh = null;
	}
	if($platform == "sc" || isset($_REQUEST['secure'])){
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass,"smartcar");
		$sql = mysqli_query($conn,"SELECT id FROM dash_users WHERE token = '".mysqli_real_escape_string($conn,$token)."' LIMIT 1") or die(mysqli_error($conn));
	}else{
		$conn = mysqli_connect($dbhost, $dbuser, $dbpass,"zeekauto");
		$sql = mysqli_query($conn,"SELECT id FROM users WHERE token = '".mysqli_real_escape_string($conn,md5($token))."' LIMIT 1") or die(mysqli_error($conn));
	}
	
	while( $row = mysqli_fetch_array($sql) ){
		$auth = true;
		$user_data = $row;
	}
	if(strpos($_REQUEST['regId'],"|Huawei") >0) {
		//ES TOKEN DE HUAWEI
		$hms = true;
		//REMOVEMOS |Huawei DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-7);
	}
	if(strpos($_REQUEST['regId'],"@ST") >0) {
		//ES TOKEN DE SMART FINDER
		//REMOVEMOS @ST DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-3);
	}
	if(strpos($_REQUEST['regId'],"@CCS") >0) {
		//ES TOKEN DE CARRO CONECTADO SEGURIDAD
		//REMOVEMOS @CCS DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-4);
	}
	if(strpos($_REQUEST['regId'],"@CCW") >0) {
		//ES TOKEN DE CARRO CONECTADO WIFI
		//REMOVEMOS @CCW DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-4);
	}
	$registrationIDs = $_REQUEST['regId'];

	if($auth == true) {
		if($platform == "gps" || $platform == "new"){
			$id = rand(1,999999);
			/* PROTO ZEEKGPS y ZEEK MI AUTO 2.2.0+  IOS & ANDROID */
			$url    = 'https://fcm.googleapis.com/fcm/send';
			if($platform == "gps"){
				$apiKey = "AAAAuZjz-8Y:APA91bHC_WWsf6GzihhTFpKqyW35igk7KmnIwFZftxD8vcMKYUPQ5BHCfrK4YXCF-VNe6RMzRdFYs43Y1oR3Kxamd9oRH0HEMcmS3OEZRPKOxeJqIhPKxQFeF3WQn1wtUjSdOxOhygsw";
				$hAppId = "102831199";
				$hAppSecret = "fb955acefaecd458c8ea8001b75948de46b262e762a17772fe51802fb47d926a";
			}
			if($platform == "new"){
				$apiKey = "AAAAXGWvNrI:APA91bEULBpkJFrZXWzCFXWUCrpa-q0G9onsoXtCWdvmz24cr-IL-CzY93iXq1w1Tbdogk5FTHNzfZiYk7yGfIhpxCz5Z6AKifyQMzJxXH3XZHNWxlfqN6mM5Nfd5_pUYvYjqPyrwZf5"; //CAMBIAR POR API KEY DE PRODUCCION DE ZEEK AUTO
				$hAppId = "103457147";	
				$hAppSecret = "25b57fae6c57bdf484f614d2fc39ff4aeeedbca865563b03b8487840c78844a0";
			}
			
			if($push_os == "android" ){
				$channel = "PushPluginChannel";
				/* ANDROID */
				$payload["title"]= urldecode($_REQUEST['title']);
				$payload["body"] = urldecode($_REQUEST['message']);
				if($_REQUEST['icon'] == "modoalarma" || $image == "modoalarma" || $payload['tipo'] == "alarma") {
					$payload["notId"] = 0;
					$channel = "modoalarma";
					$payload['image'] = 'https://zeek.imeev.com/img/modoalarma.png';
					$sonido = "modoalarma3";
				}else{
					$payload["icon"] = "";
					//$payload["style"] = "inbox";
					//$payload["summaryText"] ="%n% notificaciones";
					$payload["notId"] = $id;
					$sonido = $_REQUEST['sound'];
				}
				if($payload['tipo'] == 'agenda') {
					$payload["summaryText"] = "Recordatorio";
					if($_REQUEST['message'] == '') {
						$payload["body"] = 'Sin descripción';
					}
				}else{
					if($hms) {
						$payload["summaryText"] = "Notificación";
					}
				}
				
				$payload['android_channel_id'] = $channel;
				$payload["sound"] = $sonido;
				$payload["soundname"] = $sonido;
				if (isset($_REQUEST['image'])) {
					if($platform == "gps") {
						$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "" : "https://zeekgps.imeev.com/img/".$_REQUEST['image'] ;
					}else{
						if($_REQUEST['icon'] == "modoalarma" || $_REQUEST['image'] == "modoalarma" || $payload['tipo'] == "alarma") {
							$payload["image"] = "https://zeek.imeev.com/img/modoalarma.png";
						}else{
							$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "www/images/launcher_icon.png" : "www/images/".$_REQUEST['image'] ;
							if($payload['tipo'] == "imagen") {
								$payload["image"] = $_REQUEST['image'];
							}
							if($hms) {
								$payload["image"] = "";
							}
						}
					}
				}
				if($hms == true){
					
					$notifData = array(
						"validate_only"=>false,
						"message"=> array(
							"data"=> json_encode($payload),
							"notification"=> array(
								"title"=> 	urldecode($_REQUEST['title']),
								"body"=> 	urldecode($_REQUEST['message']),
								"image"=> 	$payload['image']
							),
							"android"=> array(
								"collapse_key"=> 	-1,
								"urgency"=> 		"HIGH",
								"ttl"=> 			"1448s",
								"bi_tag"=> 			"push_sent",
								"notification" => array(
									"foreground_show"=>false,
									"title"=> 		urldecode($_REQUEST['title']),
									"body"=> 		urldecode($_REQUEST['message']),
									"image"=> 		$payload['image'],
									"icon"=>		$payload['image'],
									"color"=> 		"#AACCDD",
									"channel_id"=> "ZeekPush",
									"notify_summary"=> $payload["summaryText"],
									"style"=> 		0,
									"big_title"=> 	urldecode($_REQUEST['title']),
									"big_body"=> 	urldecode($_REQUEST['message']),
									"auto_clear"=> 	86400000,
									"notify_id"=> 	$id,
									"importance"=> "NORMAL",
									"use_default_vibrate"=> true,
									"use_default_light"=> true,
									"visibility"=> "PUBLIC",
									"click_action"=> array(
										"type"=> 1,
										"intent"=> ($platform == "gps" ? "zeekgps" : "zeekmiauto")."://?push=".json_encode($payload)
									)
								)
							),
							"token"=> [$registrationIDs]
						)
					);
					$newResult = sendHPush($notifData,$hAppId,$hAppSecret);
					$res = array(
						"status"=>"OK",
						"Huawei"=> json_decode($newResult,false),
						"sent"=>$notifData
					);
				}else{
					$payload["image-type"] = ($payload['tipo'] == "imagen" ? "" : "circular");
					$notifData = array(
						"to"=>$registrationIDs,
						"data"=>$payload,
						"sound"=> $sonido,
						"soundname"=> $sonido,
						"content-available"=> 1,
						"priority"=>"high",
						"delay_while_idle"=> false,
						"android_channel_id"=>$channel,
						"image-type"=>($payload['tipo'] == "imagen" ? "" : "circular")
					);
					
					$newResult = sendPush($notifData,$url,$apiKey);
					$res = array(
						"status"=>"OK",
						"Android"=> json_decode($newResult,false),
						"sent"=>$notifData
					);
				}
			}
			if($push_os == "ios"){
				/* IOS */
				if($_REQUEST['icon'] == "modoalarma"  || $image == "modoalarma") {
					$sonido = "modoalarma3.wav";
				}else{
					$sonido = $_REQUEST['sound'];
				}
				$title = $_REQUEST['title'];
				$mensaje = $_REQUEST['message'];
				if($payload['tipo'] == 'agenda') {
					$title = "Recordatorio: ".urldecode($title);
				}
				$data = array(
					"to"=>$_REQUEST['regId'],
					"notification" => array(
						"body"		=> urldecode($mensaje),
						"title"		=> urldecode($title),
						'vibrate'	=> 1,
						'sound'		=> $sonido,
						'badge' 	=> 0
					),
					"content-available"=> 1,
					"content_available"=> true,
					"priority"=> "high",
					"data"=>$payload,
					"notId"=>$id,
					"force-start"=> 1,
					"delay_while_idle"=> false,
					"apns"=> array(
						"headers"=> array(
							'apns-priority'=> '10'
						),
						"payload"=> array(
							"aps"=> array(
								"sound"=>$sonido
							)
						)
					)
				);
				
				//$newResultSC = sendSCPush($platform,$push_os,$registrationIDs,$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$sonido);
				$newResult = sendPush($data,$url,$apiKey);
				$res = array(
					"status"=>"OK",
					"iOS"=> json_decode($newResult,false),
					"sent"=>$data
				);
			}
			if($push_os == "web"){
				$payload["title"]= urldecode($_REQUEST['title']);
				$payload["body"] = urldecode($_REQUEST['message']);
				$payload["gcm.message_id"] = rand(0,10000);
				if($_REQUEST['icon'] == "modoalarma" || $image == "modoalarma" || $payload['tipo'] == "alarma") {
					$payload["icon"] = "modoalarma.png";
					$payload['tag'] = 'modoalarma-push';
				}else{
					$payload["icon"] = "launcher_icon.png";
					$payload['tag'] = 'zma-push';
				}
				$webPushResult = sendPush([
					'endpoint'=>$registrationIDs,
					'authKey'=>	$authKey,
					'p256dh'=>	$p256dh,
					'payload'=> $payload
				],"https://auto.zeekgps.com/web_push/send.php",'');
				$res = array(
					"status"=>"OK",
					"WEB"=>json_decode($webPushResult,false)
				);
			}
		}
		if($platform == "sc") {
			$newResultSC = sendAppPush("SC",$hms,$platform,$push_os,$registrationIDs,$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_REQUEST['sound']);
			$res = array(
				"status"=>"OK",
				"SC"=>json_decode($newResultSC,false)
			);
		}
		if($platform == "st") {
			$newResultST = sendAppPush("ST",$hms,$platform,$push_os,$registrationIDs,$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_REQUEST['sound']);
			$res = array(
				"status"=>"OK",
				"ST"=>json_decode($newResultST,false)
			);
		}
		if($platform == "ccs") {
			$newResultCCS = sendAppPush("CCS",$hms,$platform,$push_os,$registrationIDs,$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_REQUEST['sound']);
			$res = array(
				"status"=>"OK",
				"CCS"=>json_decode($newResultCCS,false)
			);
		}
		mysqli_query($conn,"INSERT INTO sent_notifs (payload,sender,result,enviado,destinatario) VALUES ('".json_encode($payload)."','".$user_data['id']."','".json_encode($res)."',CURRENT_TIMESTAMP(),'".$destinatario."')");
	}else{
		$res = array(
			"status"=>"invalid_token"
		);
	}
	echo json_encode($res);
	
	function sendPush($data,$url,$apiKey){
		//http header
		$headers = array('Authorization: key=' . $apiKey,
					 'Content-Type: application/json');
		//curl connection
		$ch = curl_init();
		 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		 
		$result = curl_exec($ch);
		 
		curl_close($ch);
		return $result;
		
	}
	function sendAppPush($app,$hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {
		$url = "";
		switch($app) {
			case "SC": $url = "https://auto.zeekgps.com/push/push-smartcar.php"; break;
			case "ST":
			case "SF": $url = "https://auto.zeekgps.com/push/push-finder.php"; break;
			case "CCS": $url = "https://auto.zeekgps.com/push/push-ccs.php"; break;
		}
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	function sendSCPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-smartcar.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	function sendSFPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-finder.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	function sendTEPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-teapp.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	function sendHPush($data,$appId,$appSecret) {
		$tData = getHAccessToken($appId,$appSecret);
		return hPost("https://push-api.cloud.huawei.com/v1/".$appId."/messages:send", json_encode($data), array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$tData->access_token
        ));
	}
	function getHAccessToken($appId,$appSecret) {
		return json_decode(hPost("https://login.cloud.huawei.com/oauth2/v2/token", http_build_query(array(
            "grant_type" => "client_credentials",
            "client_secret" => $appSecret,
            "client_id" => $appId
        )), array(
            "Content-Type: application/x-www-form-urlencoded;charset=utf-8"
        )));
	}
	function hPost($url,$data,$header) {
		$ch = curl_init($url);
		
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        // resolve SSL: no alternative certificate subject name matches target host name
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // check verify
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1); // regular post request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post submit data

        $response = curl_exec($ch);
		$err = curl_error($ch);	
        curl_close($ch);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

	}
	mysqli_close($conn);
?>