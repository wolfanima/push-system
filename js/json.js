function json(type) {
	var json = new Object();
	json.tipo = type;
	$("#image").val("");
	switch(type) {
		case "trace":
			json.DirectionsEnabled = true;
			json.ShowTraza = true;
		break;
		case "sound":
			json.file = "beep";
		break;
		case "pollrefresh":
			json.valor = 5000;
		break;
		case "allowmarkeranimation":
			json.valor = "true";
		break;
		case "displayevent":
			json.id = 0;
			json.car = "";
			json.maxwidth = "250";
			json.maxheight = "75";
		break;
		case "rating":
			json.PorCalificar = new Array();
			json.PorCalificar.push({
				"titulo_encuesta":	"Califica tu instalación",
				"texto":			"¿Cómo calificaría el servicio de instalación de su auto NP-300 en UN Baja?",
				"unidadid":			"206678504st",
				"calificacionid":	"1"
			});
		break;
		case "toast":
			json.mensaje = "Mensaje de prueba";
		break;
		case "alarma":
			json.carid = "";
			json.lat = "";
			json.lng = "";
			json.geoid = "";
			$("#image").val("modoalarma");
		break;
		case "evento":
			json.latitud = "32.5082394";
			json.longitud = "-116.9911287";
			json.id = rand(1000,10000);
			json.unidad = "";
			json.ms = new Date().getTime();
			json.velocidad = 50;
			json.evento = 200;
			json.ng = "Geocerca";
		break;
		case "gpsevento":
			var eid = rand(0,_pushEvents.length-1);
			$("#titulo").val("Zeek GPS App");
			$("#mensaje").val(GetEvento(_pushEvents[eid]));
			$("#image").val("eventos/"+_pushEvents[eid]+".png");
			$("label[for='image']").addClass("active");
			$("#platform").val("gps");
			try{
				$('select').formSelect();
			}catch(e){}
		break;
		case "sms":
			$("#titulo").val("SmartCar");
			$("#mensaje").val("Has recibido un SMS!");
			json.id = rand(0,1000);
			json.titulo = "SmartCarSMS";
			json.unidad = "";
			json.mensaje = "Tu tarjeta de credito esta lista y aprobada! Sigue el siguiente enlace para aceptarla 1000% seguro shi cheñol: http://bit.ly/2vWTZL6";
		break;
		case "agenda":
			json.id = -1;
			json.titulo = "Evento de Recordatorio";
			json.mensaje = "Esto es una prueba";
			$("#titulo").val(json.titulo);
			$("#mensaje").val(json.mensaje);
		break;
	}
	
	$("#payload").val(JSON.stringify(json));
	try{M.textareaAutoResize($('#payload'));}catch(e){}
	try{textAreaAdjust($("#payload")[0]);}catch(e){}
}