var _pushEvents = [
        133,    //Aceleracion Intensa
        14,     //Arrastre de Vehículo
        23,     //Bateria Baja (Interna)
        10,     //Bateria Baja (Externa),
        12,     //Bateria Desconectada
        13,     //Bateria Reconectada
        2,      //Boton de Panico
        105,    //Carga Combustible
        19,     //Cofre Abierto
        20,     //Cofre Cerrado
        140,    //Colision
        47,     //Conexion Bateria Interna
        16,     //Conexion de Caja
        137,    //Curva con velocidad alta
        37,     //Descarga
        106,    //Descarga Combustible
        48,     //Desconexion Bateria Interna
        17,     //Desconexion de Caja
        33,     //Deteccion de Interferencia
        160,    //DTC Detectado
        159,    //DTC Eliminado
        21,     //Entregado
        9,      //Exceso de velocidad
        131,    //Fin de Exceso de Velocidad
        71,     //Fin de Ralentí
        134,    //Frenado Intenso
        164,    //Fuera de rango (Combustible - limite inferior)
        163,    //Fuera de rango (Combustible - limite superior)
        167,    //Fuera de rango (Horómetro)
        162,    //Fuera de rango (Odómetro)
        165,    //Fuera de rango (Posición del acelerador)
        161,    //Fuera de rango (RPM)
        166,    //Fuera de rango (Temperatura del anticongelante)
        136,    //Giro Brusco
        139,    //Impacto
        130,    //Inicio de exceso de velocidad
        70,     //Inicio de Ralenti
        45,     //Llegada
        36,     //Mezclando
        4,      //Motor Apagado
        3,      //Motor Encendido
        22,     //No Entregado
        5,      //Puerta Abierta
        6,      //Puerta Cerrada
        43,     //Puerta Lateral Abierta
        44,     //Puerta Lateral Cerrada
        103,    //Puerta Principal Abierta
        104,    //Puerta Principal Cerrada
        15,     //Ralenti
        11,     //Requiere Comunicacion
        46,     //Salida
        120,    //Servicio
        35,     //Tamper OFF
        34,     //Tamper ON
        102,    //Termo Apagado
        101,    //Termo Encendido
        254,    //Tiempo Inmovil
        107,    //Trasvase
        7,      //Valvula Abierta
        8      //Valvula Cerrada
    ];
function GetEvento(numEvento, aux) {
        switch (numEvento) {
            case "geo": return window.idioma == "en" ? "Geofence: "+aux : "Geocerca: "+aux;
            case -1: return window.idioma == "en" ? "Position" : "Posición";
            case 0: return window.idioma == "en" ? "" : "";
            case 1: return window.idioma == "en" ? "" : "";
            case 2: return window.idioma == "en" ? "Panic Alert" : "Botón de Pánico";
            case 3: return window.idioma == "en" ? "Ignition ON" : "Motor encendido";
            case 4: return window.idioma == "en" ? "Ignition OFF" : "Motor apagado";
            case 5: return window.idioma == "en" ? "Opened Door" : "Puerta abierta";
            case 6: return window.idioma == "en" ? "Closed Door" : "Puerta cerrada";
            case 7: return window.idioma == "en" ? "Opened Valve" : "Válvula abierta";
            case 8: return window.idioma == "en" ? "Closed Valve" : "Válvula cerrada";
            case 9: return window.idioma == "en" ? "Over Speed Limit" : "Exceso de velocidad";
            case 10: return window.idioma == "en" ? "Low Battery (External)" : "Batería Baja (Externa)";
            case 11: return window.idioma == "en" ? "Requires Communication" : "Requiere Comunicación";
            case 12: return window.idioma == "en" ? "Battery Disconnected" : "Batería Desconectada";
            case 13: return window.idioma == "en" ? "Battery Reconnected" : "Batería Reconectada";
            case 14: return window.idioma == "en" ? "Car Tow Alert" : "Arrastre de Vehículo";
            case 15: return window.idioma == "en" ? "Idle" : "Ralentí";
            case 16: return window.idioma == "en" ? "Tractor Cage Donnected" : "Conexión de Caja";
            case 17: return window.idioma == "en" ? "Tractor Cage Disconnected" : "Desconexión de Caja";
            //case 18: return window.idioma == "en" ? "Unload" : "Descarga";
            case 19: return window.idioma == "en" ? "Open Hood" : "Cofre abierto";
            case 20: return window.idioma == "en" ? "Closed Hood" : "Cofre cerrado";
            case 21: return window.idioma == "en" ? "Delivered" : "Entregado";
            case 22: return window.idioma == "en" ? "Not Delivered" : "No Entregado";
            case 23: return window.idioma == "en" ? "Low Battery (Internal)" : "Batería Baja (Interna)";
            case 30: return window.idioma == "en" ? "Energy Saving" : "Ahorro de energía";
            case 33: return window.idioma == "en" ? "Jamming" : "Deteccion de Interferencia";
            case 34: return window.idioma == "en" ? "Tamper ON" : "Tamper ON";
            case 35: return window.idioma == "en" ? "Tamper OFF" : "Tamper OFF";
            case 36: return window.idioma == "en" ? "Mixing" : "Mezclando";
            case 37: return window.idioma == "en" ? "Unload" : "Descarga";
            case 43: return window.idioma == "en" ? "Side Door Opened" : "Puerta Lateral Abierta";
            case 44: return window.idioma == "en" ? "Side Door Closed" : "Puerta Lateral Cerrada";
            case 45: return window.idioma == "en" ? "Arrival" : "Entrada";
            case 46: return window.idioma == "en" ? "Departure" : "Salida";
            case 47: return window.idioma == "en" ? "Internal Battery Connected" : "Conexión Batería Interna";
            case 48: return window.idioma == "en" ? "Internal Battery Disconnected" : "Desconexión Batería Interna";
            case 50: return window.idioma == "en" ? "Temperature Normal" : "Temperatura Normal";
            case 51: return window.idioma == "en" ? "Temperature Low" : "Temperatura Baja";
            case 52: return window.idioma == "en" ? "Temperature High" : "Temperatura Alta";
            case 53: return window.idioma == "en" ? "Pressure Normal" : "Presión Normal";
            case 54: return window.idioma == "en" ? "Presión Low" : "Presión Baja";
            case 55: return window.idioma == "en" ? "Presión High" : "Presión Alta";
            case 56: return window.idioma == "en" ? "Level Normal" : "Nivel Normal";
            case 57: return window.idioma == "en" ? "Level Low" : "Nivel Bajo";
            case 70: return window.idioma == "en" ? "Idle Begin" : "Inicio de Ralentí";
            case 71: return window.idioma == "en" ? "Idle End" : "Fin de Ralentí";
            case 101: return window.idioma == "en" ? "Thermo ON" : "Termo Encendido";
            case 102: return window.idioma == "en" ? "Thermo OFF" : "Termo Apagado";
            case 103: return window.idioma == "en" ? "Rear Door Opened" : "Puerta Principal Abierta";
            case 104: return window.idioma == "en" ? "Rear Door Closed" : "Puerta Principal Cerrada";
            case 105: return window.idioma == "en" ? "Fuel Load" : "Carga Combustible";
            case 106: return window.idioma == "en" ? "Fuel Discharge" : "Descarga Combustible";
            case 107: return window.idioma == "en" ? "Fuel Transfer" : "Trasvase";
            case 120: return window.idioma == "en" ? "Service" : "Servicio";		/* Added by Armando */
            case 130: return window.idioma == "en" ? "Speeding Start" : "Inicio de Exceso de Velocidad";
            case 131: return window.idioma == "en" ? "Speeding End" : "Fin de Exceso de Velocidad";
            case 133: return window.idioma == "en" ? "Intense Acceleration" : "Aceleración intensa";
            case 134: return window.idioma == "en" ? "Intense Braking" : "Frenado intenso";
            case 136: return window.idioma == "en" ? "Sharp Turn" : "Giro brusco";
            case 137: return window.idioma == "en" ? "Dangerous Curve" : "Curva con velocidad alta";
            case 139: return window.idioma == "en" ? "Impact" : "Impacto";
            case 140: return window.idioma == "en" ? "Collision" : "Colisión";
            case 142: return window.idioma == "en" ? "Excess continuous driving time" : "Exceso de tiempo continuo manejando";
            case 143: return window.idioma == "en" ? "Driving after hours: Start" : "Inicio de manejo fuera de horario";
            case 144: return window.idioma == "en" ? "Driving after hours: End" : "Fin de manejo fuera de horario";
            case 159: return window.idioma == "en" ? "DTC Removed" : "DTC Eliminado";
            case 160: return window.idioma == "en" ? "DTC Detected" : "DTC Detectado";
            case 161: return window.idioma == "en" ? "Out of range (RPM)" : "Fuera de rango (RPM)";
            case 162: return window.idioma == "en" ? "Out of range (Odometer)" : "Fuera de rango (Odómetro)";
            case 163: return window.idioma == "en" ? "Out of range (Fuel - upper limit)" : "Fuera de rango (Combustible - limite superior)";
            case 164: return window.idioma == "en" ? "Out of range (Fuel - lower limit)" : "Fuera de rango (Combustible - limite inferior)";
            case 165: return window.idioma == "en" ? "Out of range (Throttle position)" : "Fuera de rango (Posición del acelerador)";
            case 166: return window.idioma == "en" ? "Out of range (Antifreeze temperature)" : "Fuera de rango (Temperatura del anticongelante)";
            case 167: return window.idioma == "en" ? "Out of range (Horometer)" : "Fuera de rango (Horómetro)";
            case 254: return window.idioma == "en" ? "still accumulated time" : "Tiempo Inmovil Acumulado";
            default: return window.idioma == "en" ? "Undefined" + numEvento : "Indefinido" + numEvento;
        }
    }