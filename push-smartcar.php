<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	//SMARTCAR HUAWEI
	$hAppId = "103225787";	
	$hAppSecret = "c8c305dc8539a885ae1d5a035ba4980ebd34d0743b2adfdaabf7b3abb4a03694";
	$res = array();
	if(isset($_REQUEST['hms'])){
		$hms = $_REQUEST['hms'];
	}else{
		$hms = false;
	}
	if(	!isset($_REQUEST['payload']) || 
		!isset($_REQUEST['regId']) ||
		!isset($_REQUEST['title']) ||
		!isset($_REQUEST['message']) ||
		!isset($_REQUEST['icon']) ||
		!isset($_REQUEST['sound'])
	){
		$res = array(
			'status'=>'ERROR',
			'message'=>'Datos faltantes, el request debe incluir: "payload", "regId", "title", "message", "icon" y "sound".'
		);
	}else{
		if(strpos($_REQUEST['regId'],"|Huawei") >0) {
			//ES TOKEN DE HUAWEI
			$hms = true;
			//REMOVEMOS |Huawei DEL TOKEN
			$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-7);
		}
		$payload_fixed = preg_replace( "/\r|\n/", " ", $_REQUEST['payload'] );
		$payload = json_decode($payload_fixed,true);
		$registrationIDs = array($_REQUEST['regId']);
		if(isset($_REQUEST['platform'])){
			$platform = $_REQUEST['platform'];
		}else{
			$platform = "new";
		}
		if(isset($_REQUEST['image'])) {
			$image = $_REQUEST['image'];
		}else{
			$image = null;
		}
		if(isset($_REQUEST['os'])){
			$push_os = $_REQUEST['os'];
		}else{
			$push_os = "android";
		}
	
		
		$id = rand(1,999999);
		/* PROTO ZEEKGPS y ZEEK MI AUTO 2.2.0+  IOS & ANDROID */
		$url    = 'https://fcm.googleapis.com/fcm/send';

		$apiKey = "AAAA4dFLjXQ:APA91bEdJs0NLTgu5tTZgPe6CkfvN0MD28ha6APGxcdZbWGpF0jKt5WiO6OQgfDK3cd-lyi-pf8YV7JHazd3EcRMLnSndyMg7nAEp8wRa4VK8hv8w5shGZwtBgHN4o9FQFDPEqrQFGJx"; //CAMBIAR POR API KEY DE PRODUCCION DE ZEEK AUTO
		if($push_os == "android" ){
			$channel = "PushPluginChannel";
			/* ANDROID */
			$payload["title"] =urldecode($_REQUEST['title']);
			$payload["body"] = urldecode($_REQUEST['message']);
			
			if($_REQUEST['icon'] == "modoalarma" || $image == "modoalarma") {
				$payload["notId"] = 0;
				$channel = "modoalarma";
				$payload["image"] = "https://zeek.imeev.com/images/modoalarma.png";
				$sonido = "modoalarma3";
			}else{
				$payload["icon"] = "icon";
				//$payload["style"] = "inbox";
				//$payload["summaryText"] ="%n% notificaciones";
				$payload["notId"] = $id;
				$sonido =  $_REQUEST['sound'];
			}
			$payload['android_channel_id'] = $channel;
			$payload["sound"] = $sonido;
			$payload["soundname"] = $sonido;
			//$payload["icon"] = (!isset($_REQUEST['icon']) || $_REQUEST['icon'] == "") ? "launcher_icon" : $_REQUEST['icon'];
			if (isset($_REQUEST['image'])) {
				if($platform == "gps") {
					$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "" : "www/img/".$_REQUEST['image'] ;
				}else{
					if($_REQUEST['icon'] == "modoalarma" || $_REQUEST['image'] == "modoalarma") {
						$payload["image"] = "https://zeek.imeev.com/images/modoalarma.png";
					}else{
						$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "www/images/launcher_icon.png" : "www/images/".$_REQUEST['image'] ;
						if($hms) {
							$payload["image"] = "";
						}
					}				
				}
			}else{
				$payload["image"] = "";
			}
			if($payload["tipo"] == "agenda") {
				$payload["summaryText"] = "Recordatorio";
				if($_REQUEST['message'] == '') {
					$payload["body"] = 'Sin descripción';
				}
			}else{
				if($hms) {
					$payload["summaryText"] = "Notificación";
				}
			}
			
			if($hms == true){
			
				$notifData = array(
					"validate_only"=>false,
					"message"=> array(
						"data"=> json_encode($payload),
						"notification"=> array(
							"title"=> 	urldecode($_REQUEST['title']),
							"body"=> 	urldecode($_REQUEST['message']),
							"image"=> 	$payload['image']
						),
						"android"=> array(
							"collapse_key"=> 	-1,
							"urgency"=> 		"HIGH",
							"ttl"=> 			"1448s",
							"bi_tag"=> 			"push_sent",
							"notification" => array(
								"foreground_show"=>false,
								"title"=> 		urldecode($_REQUEST['title']),
								"body"=> 		urldecode($_REQUEST['message']),
								"image"=> 		$payload['image'],
								"icon"=>		$payload['image'],
								"color"=> 		"#AACCDD",
								"channel_id"=> "SmartCarPush",
								"notify_summary"=> $payload["summaryText"],
								"style"=> 		0,
								"big_title"=> 	urldecode($_REQUEST['title']),
								"big_body"=> 	urldecode($_REQUEST['message']),
								"auto_clear"=> 	86400000,
								"notify_id"=> 	$id,
								"importance"=> "HIGH",
								"use_default_vibrate"=> true,
								"use_default_light"=> true,
								"visibility"=> "PUBLIC",
								"click_action"=> array(
									"type"=> 1,
									"intent"=> "smartcar://?push=".json_encode($payload)
								)
							)
						),
						"token"=> [$_REQUEST['regId']]
					)
				);
				$newResult = sendHPush($notifData,$hAppId,$hAppSecret);
				$res = array(
					"status"=>"OK",
					"Huawei"=> json_decode($newResult,false),
					"sent"=>$notifData
				);
			}else{
				$payload["image-type"] = "circle";
				//$payload["sound"] = $_REQUEST['sound'];
				$notifData = array(
						"to"=>$_REQUEST['regId'],
						"data"=>$payload,
						"sound"=> $sonido,
						"soundname"=> $sonido,
						"content-available"=> 1,
						"priority"=>"high",
						"delay_while_idle"=> false,
						"android_channel_id"=>$channel
					);
				$newResult = sendPush($notifData,$url,$apiKey);
				
				$res = array(
					"Android"=> $newResult
				);
			}
		}
		if($push_os == "ios"){
			/* IOS */
			if($_REQUEST['icon'] == "modoalarma" || $image == "modoalarma") {
				$sonido = "modoalarma3.wav";
			}else{
				$sonido = $_REQUEST['sound'];
			}
			$title = $_REQUEST['title'];
			if($payload['tipo'] == 'agenda') {
				$title = "Recordatorio: ".urldecode($title);
			}
			$data = array(
					"to"=>$_REQUEST['regId'],
					"notification" => array(
					"icon"=> (!isset($_REQUEST['icon'])) ? "sc_launcher_icon" : (($_REQUEST['icon'] == "icon" || $_REQUEST['icon'] == '') ? "sc_launcher_icon" : $_REQUEST['icon']),
					'color' => (!isset($_REQUEST['color'])) ? '#002f87' : '#'.$_REQUEST['color'],
					"body"	=> urldecode($_REQUEST['message']),
					"title"		=> urldecode($title),
					'vibrate'	=> 1,
					'sound'		=> $sonido,
					
					'badge' => 0,
					"content-available"=> 1
				),
				"content_available"=> "true",
				"data"=>$payload,
					"content_available"=> true,
					"content-available"=> 1,
					"priority"=>"high",
					"notId"=>$id,
					"force-start"=> 1,
					"delay_while_idle"=> false
				);
			$newResult = sendPush($data,$url,$apiKey);
			
			$res = array(
				"iOS"=> $newResult
			);
		}
	}
	
	echo json_encode($res);
	
	function sendPush($data,$url,$apiKey){
		//http header
		$headers = array('Authorization: key=' . $apiKey,
					 'Content-Type: application/json');
		//curl connection
		$ch = curl_init();
		 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		 
		$result = curl_exec($ch);
		 
		curl_close($ch);
		return $result;
	}
		function sendHPush($data,$appId,$appSecret) {
		$tData = getHAccessToken($appId,$appSecret);
		return hPost("https://push-api.cloud.huawei.com/v1/".$appId."/messages:send", json_encode($data), array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$tData->access_token
        ));
	}
	function getHAccessToken($appId,$appSecret) {
		return json_decode(hPost("https://oauth-login.cloud.huawei.com/oauth2/v2/token", http_build_query(array(
            "grant_type" => "client_credentials",
            "client_secret" => $appSecret,
            "client_id" => $appId
        )), array(
            "Content-Type: application/x-www-form-urlencoded;charset=utf-8"
        )));
	}
	function hPost($url,$data,$header) {
		$ch = curl_init($url);
		
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        // resolve SSL: no alternative certificate subject name matches target host name
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // check verify
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1); // regular post request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post submit data

        $response = curl_exec($ch);
		$err = curl_error($ch);	
        curl_close($ch);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

	}
?>