<?php
	header("Access-Control-Allow-Origin: *");
	header('Content-type: text/json; charset=utf-8');
	$res = array();
	$payload = json_decode($_REQUEST['payload'],true);
	$registrationIDs = array($_REQUEST['regId']);
	if(isset($_REQUEST['platform'])){
		$platform = $_REQUEST['platform'];
	}else{
		$platform = "old";
	}
	if(isset($_REQUEST['os'])){
		$push_os = $_REQUEST['os'];
	}else{
		$push_os = "android";
	}

	if($platform == "old"){
		/* ZEEK MI AUTO PRE 2.2.0 */
		/* PUSH PARA IOS */
			$url    = 'https://fcm.googleapis.com/fcm/send';
			$id = rand(1,999999);
			$apiKey = 'AAAADiqXQO0:APA91bHWruU9yRFJqFKPM_UCkE-fJM_PWju6eFa2OVCCf2jwp9do6Gzyh-fLbFCUXY8yGQ_XucnhgYT1uF-SDkQgOoOL-QTIRtuoqrMIRyGLuJXoOWU1UMwFjnuLbXWO0MNFuFfyalnCVQgSUC7zstJRTIdgQ__DQA'; //CAMBIAR POR API KEY DE PRODUCCION DE ZEEK AUTO
			$data = array(
				"to"=>$_REQUEST['regId'],
				"notification" => array(
				"icon"=> (!isset($_REQUEST['icon'])) ? "launcher_icon" : (($_REQUEST['icon'] == "icon" || $_REQUEST['icon'] == '') ? "launcher_icon" : $_REQUEST['icon']),
				'color' => (!isset($_REQUEST['color'])) ? '#002f87' : '#'.$_REQUEST['color'],
				"body"	=> $_REQUEST['message'],
				"title"		=> $_REQUEST['title'],
				'vibrate'	=> 1,
				'sound'		=> $_REQUEST['sound'],
				'badge' => 0,
				'notId'=>$id
			),
			"data"=>$payload,
				"content_available"=> true,
				"priority"=>"high",
				"delay_while_idle"=> false
			);

		$iosPreResult = sendPush($data,$url,$apiKey);
		
		/* ANDROID */
		$url    = "https://gcm-http.googleapis.com/gcm/send";
		$apiKey = "AIzaSyDtJwY_eWSEjwRZ2CyugEZY-elEDu_Rk2A";
		$data = array(
			"to"=>$_REQUEST['regId'],
			"data" => array(
				"icon"=> (!isset($_REQUEST['icon'])) ? "launcher_icon" : (($_REQUEST['icon'] == "icon" || $_REQUEST['icon'] == '') ? "launcher_icon" : $_REQUEST['icon']),
				'color' => (!isset($_REQUEST['color'])) ? '#002f87' : '#'.$_REQUEST['color'],
				"message"	=> $_REQUEST['message'],
				"title"		=> $_REQUEST['title'],
				'vibrate'	=>1,
				'sound'		=> $_REQUEST['sound'],
				'badge' => 0,
				"notId"=> ( $payload['tipo'] == "alarma" ? 0 : $id),
				//EJEMPLO DE PICTURE PUSH
				//"style"=>"picture",
				//"picture"=>"http://i.imgur.com/uiVQrKO.jpg",
				//"summaryText"=>"Imagen revelada",
				/*"style"=>"inbox",
				"summaryText"=>"Hay %n% notificaciones pendientes",*/
				"payload"=>$payload,
				"content-available"=>"1",
				"vibrationPattern"=> [0, 500, 100, 100, 100, 100],
				//EJEMPLO DE ACTION BUTTONS
				/*"actions"=> json_decode('[
					{ "icon": "ma_icon", "title": "DESACTIVAR", "callback": "action", "foreground": false, "inline": true },
					{ "icon": "icon", "title": "APAGAR", "callback": "app.snooze", "foreground": false}
				]',true)*/
			),
			"delay_while_idle"=> false
			);
	 
		$androidPreResult = sendPush($data,$url,$apiKey);
		
		$res = array(
			"IOSPre"=>	$iosPreResult,
			"AndroidPre"=>	$androidPreResult,
			"Tipo"=> $payload['tipo']
		);
	}
	if($platform == "gps" || $platform == "new"){
		/* PROTO ZEEKGPS y ZEEK MI AUTO 2.2.0+  IOS & ANDROID */
		$url    = 'https://fcm.googleapis.com/fcm/send';
		if($platform == "gps"){
			$apiKey = "AAAAuZjz-8Y:APA91bHC_WWsf6GzihhTFpKqyW35igk7KmnIwFZftxD8vcMKYUPQ5BHCfrK4YXCF-VNe6RMzRdFYs43Y1oR3Kxamd9oRH0HEMcmS3OEZRPKOxeJqIhPKxQFeF3WQn1wtUjSdOxOhygsw";
		}
		if($platform == "new"){
				$apiKey = "AAAAXGWvNrI:APA91bEULBpkJFrZXWzCFXWUCrpa-q0G9onsoXtCWdvmz24cr-IL-CzY93iXq1w1Tbdogk5FTHNzfZiYk7yGfIhpxCz5Z6AKifyQMzJxXH3XZHNWxlfqN6mM5Nfd5_pUYvYjqPyrwZf5"; //CAMBIAR POR API KEY DE PRODUCCION DE ZEEK AUTO
		}
		if($push_os == "android" ){
			/* ANDROID */
			$payload["title"] =$_REQUEST['title'];
			$payload["body"] = $_REQUEST['message'];
			if($_REQUEST['icon'] == "modoalarma") {
				$payload["notId"] = 0;
			}else{
				//$payload["style"] = "inbox";
				//$payload["summaryText"] ="%n% notificaciones";
				$payload["notId"] = $id;
			}
			$payload["soundname"] = $_REQUEST['sound'];
			$payload["image"] =(!isset($_REQUEST['icon']) || $_REQUEST['icon'] == "") ? "" : "www/images/".$_REQUEST['icon']. ".png" ;
			$payload["image-type"] = "circular";
			$notifData = array(
					"to"=>$_REQUEST['regId'],
					"data"=>$payload,
					"content-available"=> 1,
					"priority"=>"high",
					"delay_while_idle"=> false
				);
			$newResult = sendPush($notifData,$url,$apiKey);
			
			$res = array(
				"Proto&NewAndroid"=> $newResult,
				"Tipo"=> $payload['tipo']
			);
		}
		if($push_os == "ios"){
			/* IOS */
			$data = array(
					"to"=>$_REQUEST['regId'],
					"notification" => array(
					"icon"=> (!isset($_REQUEST['icon'])) ? "launcher_icon" : (($_REQUEST['icon'] == "icon" || $_REQUEST['icon'] == '') ? "launcher_icon" : $_REQUEST['icon']),
					'color' => (!isset($_REQUEST['color'])) ? '#002f87' : '#'.$_REQUEST['color'],
					"body"	=> $_REQUEST['message'],
					"title"		=> $_REQUEST['title'],
					'vibrate'	=> 1,
					'sound'		=> $_REQUEST['sound'],
					'badge' => 0
				),
				"data"=>$payload,
					"content_available"=> true,
					"priority"=>"high",
					"delay_while_idle"=> false
				);
			$newResult = sendPush($data,$url,$apiKey);
			
			$res = array(
				"Proto&NewiOS"=> $newResult,
				"Tipo"=> $payload['tipo']
			);
		}
	}

	echo json_encode($res);
	
	function sendPush($data,$url,$apiKey){
		//http header
		$headers = array('Authorization: key=' . $apiKey,
					 'Content-Type: application/json');
		//curl connection
		$ch = curl_init();
		 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		 
		$result = curl_exec($ch);
		 
		curl_close($ch);
		return $result;
	}
?>