<?php
	header("Access-Control-Allow-Origin: *"); //CORS
	header('Content-type: text/json; charset=utf-8'); //RETORNA JSON UTF-8
	
	//ARRAY DE RESPUESTA
	$res = array();
	//REMOVEMOS SALTOS DE LINEA DEL PAYLOAD Y LOS CONVERTIMOS A ESPACIOS
	$payload_fixed = preg_replace( "/\r|\n/", " ", $_REQUEST['payload'] );
	//DECODIFICAMOS EL PAYLOAD A UN OBJETO DE PHP
	$payload = json_decode($payload_fixed,true);
	
	//CAPTURAMOS EL REGID EN UN ARREGLO
	$registrationIDs = array(urldecode($_REQUEST['regId']));
	
	//CAPTURA DE PLATAFORMA
	//GPS = ZEEKGPS
	//NEW = ZEEKMIAUTO U OTROS
	if(isset($_REQUEST['platform'])){
		$platform = $_REQUEST['platform'];
	}else{
		$platform = "new";
	}
	
	//ES HMS? (HUAWEI)
	if(isset($_REQUEST['hms'])){
		$hms = ($_REQUEST['hms'] == "true");
	}else{
		$hms = false;
	}
	
	//TIENE IMAGEN?
	if(isset($_REQUEST['image'])) {
		$image = $_REQUEST['image'];
	}else{
		$image = null;
	}
	
	//SISTEMA OPERATIVO? IOS O ANDROID
	if(isset($_REQUEST['os'])){
		$push_os = $_REQUEST['os'];
	}else{
		$push_os = "android";
	}
	
	//TIENE ICONO?
	if(isset($_REQUEST['icon'])){
		$_icon = ($_REQUEST['icon'] != "modoalarma" || $_REQUEST['icon'] != "alarma" ? "" : $_REQUEST['icon']);
	}else{
		$_icon = "";
	}
	
	//SONIDO A REPRODUCIR
	if(isset($_REQUEST['sound'])) {
		$_sound = $_REQUEST['sound'];
	}else{
		$_sound = 'default';
	}
	
	//WEBPUSH
	//LLAVE DE AUTENTICACION
	if(isset($_REQUEST['authKey'])){
		$authKey = urldecode($_REQUEST['authKey']);
	}else{
		$authKey = null;
	}
	//WEBPUSH
	//TOKEN P256DH
	if(isset($_REQUEST['p256dh'])){
		$p256dh = urldecode($_REQUEST['p256dh']);
	}else{
		$p256dh = null;
	}
	
	//VALORES PREDETERMINADOS
	$isTrackerApp = false; 	//SMART TRACK?
	$isTEApp = false;		//TRANSPORTE EMPRESARIAL?
	$isWhitelabel = false;
	
	//SI EL REGID CONTIENE "|Huawei" ES HMS
	if(strpos($_REQUEST['regId'],"|Huawei") >0) {
		//ES TOKEN DE HUAWEI
		$hms = true;
		//REMOVEMOS |Huawei DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-7);
	}
	
	//SI EL REGID CONTIENE "@ST" ES SMART TRACK
	if(strpos($_REQUEST['regId'],"@ST") >0) {
		//ES TOKEN DE SMART TRACK
		$isTrackerApp = true;
		//REMOVEMOS @ST DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-3);
	}
	//SI EL REGID CONTIENE "@CCS" ES CARRO CONECTADO SEGURIDAD
	if(strpos($_REQUEST['regId'],"@CCS") >0) {
		//ES TOKEN DE CCS
		$isWhitelabel = true;
		$whitelabelId = 'CCS';
		//REMOVEMOS @CCS DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-4);
	}
	
	
	//SI EL REGID CONTIENE "@TE" ES TRANSPORTE EMPRESARIAL (NO SE USA)
	if(strpos($_REQUEST['regId'],"@TE") >0) {
		//ES TOKEN DE TRANSPORTE EMPRESARIAL
		$isTEApp = true;
		//REMOVEMOS @TE DEL TOKEN
		$_REQUEST['regId'] = substr($_REQUEST['regId'],0,strlen($_REQUEST['regId'])-3);
	}
	
	//ID DE PUSH RANDOM
	$id = rand(1,999999);
	
	/* PROTO ZEEKGPS y ZEEK MI AUTO 2.2.0+  IOS & ANDROID */
	$url    = 'https://fcm.googleapis.com/fcm/send';

	if($platform == "gps"){ //ZEEKGPS
		$apiKey = "AAAAuZjz-8Y:APA91bHC_WWsf6GzihhTFpKqyW35igk7KmnIwFZftxD8vcMKYUPQ5BHCfrK4YXCF-VNe6RMzRdFYs43Y1oR3Kxamd9oRH0HEMcmS3OEZRPKOxeJqIhPKxQFeF3WQn1wtUjSdOxOhygsw";
		$hAppId = "102831199"; //APPID DE HUAWEI
		$hAppSecret = "fb955acefaecd458c8ea8001b75948de46b262e762a17772fe51802fb47d926a"; //APPSECRET DE HUAWEI
	}else{ //ZEEK MI AUTO
		$apiKey = "AAAAXGWvNrI:APA91bEULBpkJFrZXWzCFXWUCrpa-q0G9onsoXtCWdvmz24cr-IL-CzY93iXq1w1Tbdogk5FTHNzfZiYk7yGfIhpxCz5Z6AKifyQMzJxXH3XZHNWxlfqN6mM5Nfd5_pUYvYjqPyrwZf5";
		$hAppId = "103457147";	//APPID DE HUAWEI
		$hAppSecret = "25b57fae6c57bdf484f614d2fc39ff4aeeedbca865563b03b8487840c78844a0"; //APPSECRET DE HUAWEI
	}

	if($push_os == "android" ){ //ES ANDROID
		$channel = "PushPluginChannel"; //NOMBRE DEL CANAL DEFAULT
		$payload["title"] =urldecode($_REQUEST['title']); //TITULO
		$payload["body"] = urldecode($_REQUEST['message']); //MENSAJE O SUBTITULO
		
		//SI HAY REFERENCIAS A MODO ALARMA EN EL ICONO, IMAGEN O EN EL TIPO DE PAYLOAD
		//SE ASIGNA LA IMAGEN DE MODO ALARMA AL IGUAL QUE EL SONIDO DE ALARMA
		if($_icon == "modoalarma" || $image == "modoalarma" || $payload['tipo'] == "alarma") {
			$_REQUEST['image'] = "https://zeek.imeev.com/img/modoalarma.png";
			$payload["notId"] = 0;
			$channel = "modoalarma";
			$payload["image"] = "https://zeek.imeev.com/img/modoalarma.png";
			$sonido = "modoalarma3";
		}else{
			$payload["icon"] = "";
			$payload["notId"] = $id;
			$sonido =  $_sound;
		}
		//TIENE SUBTEXTO?
		if(isset($_REQUEST['summary'])) {
			$payload["summaryText"] = urldecode($_REQUEST['summary']);
		}
		//ES PUSH DE AGENDA?
		if($payload['tipo'] == 'agenda') {
			//MARCAR SUBTEXTO COMO TAL
			$payload["summaryText"] = "Recordatorio";
			if($_REQUEST['message'] == '') {
				//NO TIENE DESCRIPCION ENTONCES LO MARCA COMO TAL
				$payload["body"] = 'Sin descripción';
			}
		}else{
			if($hms) {
				//EN HUAWEI SIEMPRE DEBE IR EL SUMMARYTEXT
				$payload["summaryText"] = "Notificación";
			}
		}
		$payload['android_channel_id'] = $channel;	//CANAL (CATEGORIA)
		$payload["sound"] = $sonido;				//NOMBRE DE SONIDO
		$payload["soundname"] = $sonido;			//NOMBRE DE SONIDO (ALT)
		$payload["icon"] = ($_icon == "" ? "" : $_icon); //ICONO
		
		//TIENE IMAGEN?
		if (isset($_REQUEST['image'])) {
			//IMAGEN DE EVENTOS PARA ZGPS
			if($platform == "gps") {
				$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "" : "www/img/".$_REQUEST['image'] ;
			}else{
				//CONSIDERACIONES PARA MODO ALARMA (ZMA)
				if($_icon == "modoalarma" || $_REQUEST['image'] == "modoalarma" || $payload['tipo'] == "alarma") {
					$payload["image"] = "https://zeek.imeev.com/img/modoalarma.png";
				}else{
					//CONSIDERACIONES PARA IMAGEN QUE NO ES MODO ALARMA
					$payload["image"] =(!isset($_REQUEST['image']) || $_REQUEST['image'] == "") ? "" : "www/images/".$_REQUEST['image'] ;
					if($hms) {
						$payload["image"] = "";
					}
				}				
			}
		}else{
			//SI NO MANDAMOS EL PARAMETRO VACIO
			$payload["image"] = "";
		}
		//ES HMS? (HUAWEI)
		if($hms == true){
			//ES SMART TRACK?
			if($isTrackerApp == true) {
				$newResultST = sendAppPush("ST",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"Huawei_ST"=> json_decode($newResultST,false)
				);
			}else if($isTEApp == true) { //ES TRANSPORTE EMPRESARIAL?
				$newResultTE = sendAppPush("TEA",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"Huawei_TE"=> json_decode($newResultTE,false)
				);
			}else if($isWhitelabel == true) {
				$newResultWhitelabel = sendAppPush($whitelabelId,$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"Huawei_{$whitelabelId}"=> json_decode($newResultWhitelabel,false)
				);
			}else{
				//NO ES NI SMART TRACK NI TEAPP
				//EL PAYLOAD DE HUAWEI ES UN DESMADRE, ES TAL COMO DECIA LA DOCUMENTACION
				$notifData = array(
					"validate_only"=>false,
					"message"=> array(
						"data"=> json_encode($payload),
						"notification"=> array(
							"title"=> 	urldecode($_REQUEST['title']),
							"body"=> 	urldecode($_REQUEST['message']),
							"image"=> 	$payload['image']
						),
						"android"=> array(
							"collapse_key"=> 	-1,
							"urgency"=> 		"HIGH",
							"ttl"=> 			"1448s",
							"bi_tag"=> 			"push_sent",
							"notification" => array(
								"foreground_show"=>false,
								"title"=> 		urldecode($_REQUEST['title']),
								"body"=> 		urldecode($_REQUEST['message']),
								"image"=> 		$payload['image'],
								"icon"=>		$payload['image'],
								"color"=> 		"#AACCDD",
								"channel_id"=> "ZeekPush",
								"notify_summary"=> $payload["summaryText"],
								"style"=> 		0,
								"big_title"=> 	urldecode($_REQUEST['title']),
								"big_body"=> 	urldecode($_REQUEST['message']),
								"auto_clear"=> 	86400000,
								"notify_id"=> 	$id,
								"importance"=> "HIGH",
								"use_default_vibrate"=> true,
								"use_default_light"=> true,
								"visibility"=> "PUBLIC",
								"click_action"=> array(
									"type"=> 1,
									"intent"=> ($platform == "gps" ? "zeekgps" : "zeekmiauto")."://?push=".json_encode($payload)
								)
							)
						),
						"token"=> [$_REQUEST['regId']]
					)
				);
				//SE ASUME QUE ES ZMA O SC, SE MANDAN IGUAL
				$newResult = sendHPush($notifData,$hAppId,$hAppSecret);
				$newResultSC = sendAppPush("SC",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"Huawei_ZMA"=> json_decode($newResult,false),
					"Huawei_SC"=> json_decode($newResultSC,false),
					"sent"=>$notifData
				);
			}
		}else{
			//ES FIREBASE
			$payload["image-type"] = "circle";
			$notifData = array(
				"to"=>			$_REQUEST['regId'],
				"data"=>		$payload,
				"sound"=> 		$sonido,
				"soundname"=> 	$sonido,
				"priority"=>	"high",
				"android_channel_id"=>$channel
			);
			//ES SMART TRACK ANDROID?
			if($isTrackerApp == true) {
				$newResultST = sendAppPush("ST",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"Finder_Android"=>$newResultST
				);

			}else if($isTEApp == true){ //ES TRANSPORTE EMPRESARIAL ANDROID?
				$newResultTE = sendAppPush("TEA",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"TE_Android"=>$newResultTE
				);
			}else if($platform == "gps") { //ES ZEEKGPS ANDROID?
				$newResult = sendPush($notifData,$url,$apiKey);
				$res = array(
					"ZeekGPS_Android"=> $newResult
				);
			}else if($isWhitelabel == true) {
				$newResultWhitelabel = sendAppPush($whitelabelId,$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"{$whitelabelId}_Android"=> json_decode($newResultWhitelabel,false)
				);
			}else{ 
				//ES ZMA O SMARTCAR
				$newResult = sendPush($notifData,$url,$apiKey);
				$newResultSC = sendAppPush("SC",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"MiAuto_Android"=> $newResult,
					"SmartCar_Android"=>$newResultSC
				);
			}
		}
	}
	//ES IOS?
	if($push_os == "ios"){
		//ES MODO ALARMA? SI ES ASI, DECLARAMOS EL SONIDO ADECUADO
		if($_icon == "modoalarma" || $image == "modoalarma" || $payload['tipo'] == "alarma") {
			$sonido = "modoalarma3.wav";
		}else{
			$sonido = $_sound;
		}
		$title = $_REQUEST['title'];
		//ES EVENTO AGENDA?
		if($payload['tipo'] == 'agenda') {
			//PREFIJO "Recordatorio" YA QUE NO HAY summaryText EN IOS
			$title = "Recordatorio: ".urldecode($title);
		}
		if($isTrackerApp == true) { //ES SMART TRACK IOS?
			$newResultST = sendAppPush("ST",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
			$res = array(
				"Finder_iOS"=>$newResultST
			);
		}else if($isTEApp == true) { //ES TRANSPORTE EMPRESARIAL IOS?
			$newResultTE = sendAppPush("TEA",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
			$res = array(
				"TE_iOS"=>$newResultTE
			);
		}else if($isWhitelabel == true) {
				$newResultWhitelabel = sendAppPush($whitelabelId,$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"status"=>"OK",
					"{$whitelabelId}_iOS"=> json_decode($newResultWhitelabel,false)
				);
		}else{
			//ES ZMA, SMARTCAR O ZGPS
			$data = array(
				"to"=>$_REQUEST['regId'],
				"notification" => array( //ARREGLO DE NOTIFICACION
					"icon"		=> 	(!isset($_REQUEST['icon'])) ? "icon" : (($_icon == "icon" || $_icon == '') ? "icon" : $_icon),
					'color' 	=> 	(!isset($_REQUEST['color'])) ? '#002f87' : '#'.$_REQUEST['color'],
					"body"		=> 	urldecode($_REQUEST['message']),
					"title"		=> 	urldecode($title),
					'vibrate'	=> 	1, //1 = SI, 0 = NO
					'sound'		=> 	$sonido,
					'badge' 	=> 	0 //ES EL NUMERO DE NOTIFS A MOSTRAR SOBRE EL ICONO
				),
				"data"=>				$payload,
				"priority"=>			"high",
				"notId"=>				$id,
				"force-start"=> 		1
			);
			if($platform == "gps") { //ES ZEEKGPS IOS?
				$newResult = sendPush($data,$url,$apiKey);
				$res = array(
					"ZeekGPS_iOS"=> $newResult
				);
			}else{
				//ES ZMA O SMARTCAR IOS
				$newResult = sendPush($data,$url,$apiKey);
				$newResultSC = sendAppPush("SC",$hms,$platform,$push_os,$_REQUEST['regId'],$_REQUEST['payload'],$_REQUEST['title'],$_REQUEST['message'],$image,$_sound);
				$res = array(
					"MiAuto_iOS"=> $newResult,
					"SmartCar_iOS"=>$newResultSC
				);
			}
		}
	}
	//WEB PUSH
	if($push_os == "web"){
		$payload["title"]= urldecode($_REQUEST['title']);
		$payload["body"] = urldecode($_REQUEST['message']);
		$payload["gcm.message_id"] = rand(0,10000);
		if($_icon == "modoalarma" || $image == "modoalarma" || $payload['tipo'] == "alarma") {
			$payload["icon"] = 	"modoalarma.png";
			$payload['tag'] = 	'modoalarma-push';
		}else{
			$payload["icon"] = 	"launcher_icon.png";
			$payload['tag'] = 	'zma-push';
		}
		$webPushResult = sendPush([
			'endpoint'=>urldecode($_REQUEST['regId']),
			'authKey'=>	$authKey,
			'p256dh'=>	$p256dh,
			'payload'=> $payload
		],"https://auto.zeekgps.com/web_push/send.php",'');
		$res = array(
			"status"=>"OK",
			"WEB"=>json_decode($webPushResult,false)
		);
	}
	echo json_encode($res);
	
	//ENVIO DE PUSH A ZMA O CUALQUIER NO ASIGNADO EN "plat"
	function sendPush($data,$url,$apiKey){
		//AUTH KEY EN EL HEADER
		$headers = array('Authorization: key=' . $apiKey,
					 'Content-Type: application/json');

		$ch = curl_init();
		 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		$result = curl_exec($ch);
		 
		curl_close($ch);
		return $result;
	}
	function sendAppPush($app,$hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {
		$url = "";
		switch($app) {
			case "SC": $url = "https://auto.zeekgps.com/push/push-smartcar.php"; break;
			case "ST":
			case "SF": $url = "https://auto.zeekgps.com/push/push-finder.php"; break;
			case "CCS": $url = "https://auto.zeekgps.com/push/push-ccs.php"; break;
		}
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	//ENVIO DE PUSH A SMARTCAR
	function sendSCPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-smartcar.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	//ENVIO DE PUSH A SMART TRACK
	function sendSFPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-finder.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  //LO MANDAMOS COMO UN GET YA QUE HUBO COMPLICACIONES CON UN ARRAY
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	//ENVIO DE PUSH A TRANSPORTE EMPRESARIAL
	function sendTEPush($hms,$platform,$os,$regId,$payload,$title,$message,$icon,$sound) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://auto.zeekgps.com/push/push-teapp.php",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  //LO MANDAMOS COMO UN GET YA QUE HUBO COMPLICACIONES CON UN ARRAY
		  CURLOPT_POSTFIELDS => "hms=$hms&platform=$platform&os=$os&regId=$regId&payload=$payload&title=$title&message=$message&icon=$icon&sound=$sound",
		  CURLOPT_HTTPHEADER => array(
			"Content-Type: application/x-www-form-urlencoded",
			"Postman-Token: 433b9448-b7a2-4fed-9b40-ff8f69496cde",
			"cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}
	}
	//ENVIAR PUSH HUAWEI
	function sendHPush($data,$appId,$appSecret) {
		$tData = getHAccessToken($appId,$appSecret);
		return hPost("https://push-api.cloud.huawei.com/v1/".$appId."/messages:send", json_encode($data), array(
            "Content-Type: application/json",
            "Authorization: Bearer ".$tData->access_token
        ));
	}
	//OBTENCION DE ACCESSTOKEN PARA ENVIAR PUSH HUAWEI
	function getHAccessToken($appId,$appSecret) {
		return json_decode(hPost("https://oauth-login.cloud.huawei.com/oauth2/v2/token", http_build_query(array(
            "grant_type" => "client_credentials",
            "client_secret" => $appSecret,
            "client_id" => $appId
        )), array(
            "Content-Type: application/x-www-form-urlencoded;charset=utf-8"
        )));
	}
	//POST PARA ENVIAR PUSH HUAWEI
	function hPost($url,$data,$header) {
		$ch = curl_init($url);
		
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        
        // resolve SSL: no alternative certificate subject name matches target host name
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // check verify
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_POST, 1); // regular post request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // Post submit data

        $response = curl_exec($ch);
		$err = curl_error($ch);	
        curl_close($ch);
		
		if ($err) {
		  return "cURL Error #:" . $err;
		} else {
		  return $response;
		}

	}
?>