<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <!--<meta name="viewport" id="viewport" content="target-densitydpi=device-dpi,width=device-width,height=device-height,initial-scale=1.0,user-scalable=no" />-->
        <meta name="viewport" id="viewport" content="initial-scale=1.0, width=device-width"/>
        <meta name="HandheldFriendly" content="true" />
        <meta name="MobileOptimized" content="320">
        <link rel="icon" type="image/png" href="https://uabc.imeev.com/zeek/images/headLogoSimple.png">
        <title>Zeek Mi Auto - Google Firebase Cloud Messaging</title>
        <link href="https://uabc.imeev.com/zeek/css/style.css" media="all" rel="stylesheet" />
        <link rel="stylesheet" href="https://uabc.imeev.com/zeek/css/toastr.css" media="all" />
        <link rel="stylesheet" href="https://uabc.imeev.com/zeek/css/fonts.css" media="all" />
        <script src="https://uabc.imeev.com/zeek/js/jquery.min.js" type="text/javascript"></script>
        <script src="https://uabc.imeev.com/zeek/js/toastr.js"></script>
        <script>
			toastr.options = {
			  "closeButton": false,
			  "debug": false,
			  "newestOnTop": true,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": true,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "2000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		</script>
        <style>
			body,html
			{
				font-family:'Myriad',Arial,sans-serif;	
				overflow:auto !important;
			}
			.inputBox
			{
				display:inline-block	
			}
			.button
			{
				width:100%;	
			}
			.inputBox .inputIcon
			{
				padding:0;	
			}
        </style>
        <script>
			function sendPushNotif() {
				$.ajax({
					url:"push.php",
					type: "POST",
					data: {
						os:$("#os").val(),
						title: $(".titulo").val(),
						message:$(".mensaje").val(),
						vibrate:$(".vibrate").val(),
						sound:$(".sound").val(),
						regId:$(".device").val(),
						icon:$(".icon").val(),
						platform:$("#platform").val(),
						payload:$("#payload").val()
					},
					success: function(data) {
						console.log(data);
						toastr.info("Mensaje enviado.");
					}
				});
			}
			$(document).ready(function(){
				var android = false;
				$.ajax({
					url:'https://uabc.imeev.com/zeek/api/getUsers',
					type:'GET',
					success:function(data){
						$(".device").append($("<option></option>")
						.attr("disabled",true)
						.attr("style","background:rgba(0,0,0,0.5);color:#FFF")
						.text("iOS")
						);
						for(var i=0;i<data.length;i++){
							if(data[i].platform == "android" && android == false){
								android = true;
								$(".device").append($("<option></option>")
								.attr("disabled",true)
								.attr("style","background:rgba(0,0,0,0.5);color:#FFF")
								.text("Android")
								);
							}
							$(".device").append($("<option></option>")
								.text(data[i].info)
								.val(data[i].regID)
							);
						}
					}
				});
			});
		</script>
    </head>

<body style="overflow:auto !important;">
<div class="flex">
<div class="loginLogo"></div>
<div>Google Cloud Messaging</div><table>
	    <tr>
            <td colspan="2"><input type="button" value="Enviar Mensaje" class="button" onClick="sendPushNotif()"/><br><a href="zeekmiauto://?tipo=alarma&unidad=207226122st">Custom URL Test</a></td>
        </tr>
    	<tr>
        	<td width="25%">Titulo:</td>
            <td width="75%"><span class=""><input type="text" name="titulo" class="titulo input" value="Zeek Mi Auto"/></span></td>
        </tr>
        <tr>
        	<td>Mensaje:</td>
            <td><div class=""><input type="text" name="mensaje" class="mensaje input" value="Ha recibido una notificación!"/></div></td>
        </tr>
        <tr>
        	<td>Sonido:</td>
            <td><div class=""><input type="text" name="sonido" class="sound input" value="default"/></div></td>
        </tr>
		<tr>
        	<td>Plataforma:</td>
            <td>	
				<select type="select" id="platform" class="input">
					<option value="old">Zeek Mi Auto Pre 2.2.0</option>
					<option value="new">Zeek Mi Auto 2.2.0+</option>
					<option value="gps">Zeek GPS</option>
				</select>
            </td>
        </tr>
		<tr>
        	<td>Sistema Operativo:</td>
            <td>	
				<select type="select" id="os" class="input">
					<option value="android">Android</option>
					<option value="ios">iOS</option>
				</select>
            </td>
        </tr>
		<tr>
        	<td>Icono</td>
            <td><div class=""><input type="text" name="icono" class="icon input" value="launcher_icon"/></div>
            </td>
        </tr>
        <tr>
        	<td>Payload:</td>
            <td><div class=""><textarea class="input payload" id="payload" rows="5" placeholder="Solo JSON">{"tipo":"test"}</textarea></div>
            <div>Notificacion: "tipo":"notif"<br/>Noticia: "tipo":"news"<br/>Modo Alarma: "tipo":"alarma"</div>
            </td>
        </tr>

        <tr>
        	<td>Dispositivo:</td>
            <td><div class=""><select class="device input" name="device"></select></div></td>
        </tr>

    </table>
    </div>
</body>
</html>