<?php 
	include 'opendb.php';
	$conn = mysqli_connect($dbhost, $dbuser, $dbpass,"zeekauto");
	
	$user = $_REQUEST['user'];
	$q = mysqli_query($conn, "SELECT sn.enviado, sn.result,sn.destinatario,u.nombre
								FROM sent_notifs AS sn LEFT JOIN users AS u ON u.id = sn.sender 
								WHERE sn.destinatario LIKE '%".mysqli_real_escape_string($conn, $user)."%' 
								ORDER BY sn.id LIMIT 25");
	echo '<table class="">
			<thead>
				<tr>
					<th>
						Fecha y Hora
					</th>
					<th>
						Resultado
					</th>
					<th>
						Destino
					</th>
					<th>
						Enviado Por:
					</th>
				</tr>
			</thead>
			<tbody>';
	if(mysqli_num_rows($q) == 0){
		echo '	<tr>
					<td colspan="4">Sin Resultados</td>
				</tr>';
	}else{
		while($r = mysqli_fetch_array($q)) {
			list($enviado, $resultado,$destino,$enviado_por) = $r;
			echo "	<tr>
						<td>{$enviado}</td>
						<td>{$resultado}</td>
						<td>{$destino}</td>
						<td>{$enviado_por}</td>
					</tr>";
		}
	}
	echo '	</tbody>
		</table>';
	mysqli_close($conn);
?>